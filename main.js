var express = require('express');
var path = require('path');
var app = module.exports = express();

app.set('views',path.join(__dirname,'views'));
app.set('view engine','jade');

// Routing
app.get('/partials/:name',function(req,res) {
    res.render('partials/'+req.params.name);
});

app.get('*', function (req, res) {
  res.render('layout');
});

app.get('/', function (req, res) {
  res.render('layout');
});
